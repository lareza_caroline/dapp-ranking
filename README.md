# Dapp Ranking

The Dapp Ranking is an index to assess the health and growth potential of dapp projects that powered by Dapp.com. We bring together and normalize all the important dapp metrics, such as activity level, timeliness, the profitability of the contracts, 